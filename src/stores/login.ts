import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";

export const useLoginStore = defineStore("login", () => {
  const userStore = useUserStore();
  const messageStore = useMessageStore();
  const loginName = ref("");
  const isLogin = computed(() => {
    // loginname is not empty
    return loginName.value !== "";
  });
  const login = (username: string, password: string): void => {
    if (userStore.login(username, password)) {
      loginName.value = username;
      localStorage.setItem("loginname", username);
    } else {
      messageStore.showMessage("Login หรือ Password ไม่ถูกต้อง");
    }
  };
  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("loginname");
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginname") || "";
  };

  return { LoginName: loginName, isLogin, login, logout, loadData };
});
