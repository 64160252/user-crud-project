export default interface user {
  id: number;
  login: string;
  name: string;
  password: string;
}
